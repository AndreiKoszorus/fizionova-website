import { Injectable } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  calendarURL = 'http://localhost:8080/calendar';
  userURL = 'http://localhost:8080/user';
  constructor(private httpClient: HttpClient) { }

  getInSync():Observable<any>{
    return this.httpClient.get(this.calendarURL + '/sync_events',{observe: 'response'});
  }

  insertEvent(transmittedEvent):Observable<any>{
    return this.httpClient.post(this.calendarURL + '/insert_event',transmittedEvent,{observe: 'response'});
  }

  test():Observable<any>{
    return this.httpClient.post(this.userURL + '/test','');
  }


}
