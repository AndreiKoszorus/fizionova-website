import {Component, OnInit} from '@angular/core';
import {AppService} from '../main-page/app.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SpecializationService} from '../services/specialization.service';
import {MatSnackBar} from '@angular/material';
import {UserService} from '../services/user.service';
import {FacilityDTO} from '../dto/facility-dto';
import {FacilityService} from '../services/facility.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateY(-20%)', opacity: 0}),
          animate('300ms', style({transform: 'translateY(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateY(0)', opacity: 1}),
          animate('300ms', style({transform: 'translateY(-15%)', opacity: 0}))
        ])
      ]
    )
  ],
})

export class AdminComponent implements OnInit {



  addUser:boolean = false;
  canEditUser:boolean = false;
  saveFacility:boolean = false;
  canEditFacility:boolean = false;
  canOpenSaveSpeciality:boolean = false;
  editUserFormTransitionState;
  saveUserForm:FormGroup;
  editUserForm:FormGroup;
  saveFacilityForm:FormGroup;
  editFacilityForm:FormGroup;
  saveSpecialityForm:FormGroup;
  specs = [];
  roles = ["ADMIN", "USER"];
  userDTO = {
    username:"",
    specialization:"",
    calendarId:"",
    role:""
  };

  user = {
    username:"",
    password:"",
    specialization:"",
    calendarId:"",
    active:true,
    role:""
  };

  facilities = [];
  selectedFile;
  newFile;
  facilityToEdit;

  users = [];

  constructor(private appService: AppService, private sanitizer:DomSanitizer, private formBuilder:FormBuilder, private specializationService:SpecializationService, private snackBar:MatSnackBar, private userService:UserService, private facilityService:FacilityService) { }

  ngOnInit(){
  this.saveUserForm = this.formBuilder.group({
      usernameControl: ['', Validators.required],
      passwordControl: ['',Validators.required],
      calendarIDControl:['',Validators.required],
      specializationControl: ['',Validators.required],
      roleControl:['',Validators.required]
    });
    this.editUserForm = this.formBuilder.group({
      usernameControl: [{disabled: true, value:"Username"}],
      calendarIDControl:['',Validators.required],
      specializationControl: ['',Validators.required],
      roleControl:['',Validators.required]
    });

    this.saveFacilityForm = this.formBuilder.group({
      titleControl:['',Validators.required],
      descriptionControl:['',Validators.required],
      priceControl:['',Validators.required],
      imageControl:['',Validators.required]
    });

    this.editFacilityForm = this.formBuilder.group({
      titleControl:['',Validators.required],
      descriptionControl:['',Validators.required],
      priceControl:['',Validators.required],
      imageControl:['']
    });

    this.saveSpecialityForm = this.formBuilder.group({
      nameControl: ['', Validators.required]
    });

    this.userService.getUsers().subscribe(response =>{
      if(response){
        this.users = response;
        console.log(response);
      }
    });

    this.specializationService.getSpecializations().subscribe(response =>{
      if(response){
        if(response.status == 200){
          for(let i=0; i< response.body.length;i++) {
            this.specs.push(response.body[i]);
          }
        }
        else{
          this.openSnackBar("A apărut o erorare cu codul"+response.status,'');
        }
      }
    });

    this.facilityService.getAllFacilities().subscribe(response =>{
      if(response){
        if(response.status == 200){
          this.facilities = response.body;
          console.log(this.facilities);

        }
        else{
          this.openSnackBar("A apărut o erorare cu codul"+response.status,'');
        }

      }
    });
    console.log(this.specs);

  }

  transformImage(img):SafeResourceUrl{
   return this.sanitizer.bypassSecurityTrustResourceUrl('data:image/*;base64,'+img);
  }

  openSaveUserForm(){
    this.addUser = true;
  }
  submitSaveUserForm(canContinue){
    this.user.username = this.saveUserForm.controls.usernameControl.value;
    this.user.password = this.saveUserForm.controls.passwordControl.value;
    this.user.specialization = this.saveUserForm.controls.specializationControl.value;
    this.user.calendarId = this.saveUserForm.controls.calendarIDControl.value;
    this.user.role = this.saveUserForm.controls.roleControl.value;
    this.userService.insertUser(this.user).subscribe(response =>{
      if(response){
        if(response.status == 200){
          if(response.body == 0){
            this.openSnackBar('Utilizatorul a fost inserat cu succes','');
          }
          else if(response.body == 2){
            this.openSnackBar('Exista un utilizator cu acest username','');
          }
        }
        else{
          this.openSnackBar('A aparut o eroare','');
        }
      }
      this.addUser = canContinue;
      this.saveUserForm.reset();
    });
  }
  cancelForm(){
    this.addUser = false;
    this.saveUserForm.reset();
  }
  deleteUser(user, i){
    let snackBarRef = this.snackBar.open('Sunteti sigur ca vreti sa stergeti utilizatorul?','De acord',{duration: 2000, panelClass: ['blue-snackbar']});
    snackBarRef.onAction().subscribe(() => {
      console.log('The snack-bar action was triggered!');
      this.userService.deleteUser(user.username).subscribe(response=>{
        if(response){
          if(response.status == 200){
            if(response.body == true){
              this.openSnackBar('Utilizatorul a fost sters cu succes','');
              this.users.splice(i,1);
            }
            else{
              this.openSnackBar('Nu exista utilizatorul cu acest username','')
            }
          }
          else{
            this.openSnackBar('A aparut o eroare cu codul'+response.status,'');
          }
        }
      });
    });
  }

  openSaveServiceForm(){
    this.saveFacility = true;
  }

  cancelSaveFacilityForm(){
    this.saveFacility = false;
    this.saveFacilityForm.reset();
  }


  editUser(user){
    this.canEditUser = true;
    this.editUserFormTransitionState = ':enter';
    this.editUserForm.controls.usernameControl.setValue(user.username);
    this.editUserForm.controls.calendarIDControl.setValue(user.calendarId);
    this.editUserForm.controls.specializationControl.setValue(user.specialization.name);
    this.editUserForm.controls.roleControl.setValue(user.role.role);
  }

  submitUserEditForm(){
    this.userDTO.username = this.editUserForm.controls.usernameControl.value;
    this.userDTO.calendarId = this.editUserForm.controls.calendarIDControl.value;
    this.userDTO.specialization = this.editUserForm.controls.specializationControl.value;
    this.userDTO.role = this.editUserForm.controls.roleControl.value;
    this.userService.updateUser(this.userDTO.username,this.userDTO).subscribe(response=>{
      if(response){
        if(response.status == 200){
          this.openSnackBar('Utilizatorul a fost actualizat cu succes','');
        }
        else{
          this.openSnackBar('A aparut o eroare cu codul'+response.status,'');
        }
      }
    });
    this.canEditUser = false;
    this.editUserForm.reset();
  }

  cancelEditForm(){
    this.canEditUser = false;
    this.editUserForm.reset();
  }

  onFileInput(event) {
    this.selectedFile = event.target.files[0];
  }

  uploadFacility(canContinue){
    let formData = new FormData();
    formData.append("image", this.selectedFile);

    let facilityToSend = new FacilityDTO(this.saveFacilityForm.controls.priceControl.value,this.saveFacilityForm.controls.titleControl.value, this.saveFacilityForm.controls.descriptionControl.value);
    this.facilityService.saveImage(formData).subscribe(response =>{
      if(response){
        this.facilityService.saveFacility(facilityToSend).subscribe(response =>{
          if(response){
            if(response.status == 200){
              this.openSnackBar('Serviciul a fost introdus cu succes','');
            }
            else{
              this.openSnackBar('A aparut o erorare cu codul '+response.status,'');
            }
              this.saveFacility = canContinue;
              this.saveFacilityForm.reset();
          }
        })
      }
    })
  }



  editFacility(facility){
    this.canEditFacility = true;
    this.facilityToEdit = facility;
    this.editFacilityForm.controls.titleControl.setValue(facility.title);
    this.editFacilityForm.controls.descriptionControl.setValue(facility.description);
    this.editFacilityForm.controls.priceControl.setValue(facility.price);

  }
  cancelEditFacilityForm(){
    this.canEditFacility = false;
    this.editFacilityForm.reset();
  }

  onFileEditInput(event){
    this.newFile = event.target.files[0];
  }

  uploadEditedFacility(){
    let facilityDTO  = new FacilityDTO(this.editFacilityForm.controls.priceControl.value,this.editFacilityForm.controls.titleControl.value,this.editFacilityForm.controls.descriptionControl.value);
    console.log(facilityDTO);
    if(this.editFacilityForm.controls.imageControl.value==""){
      this.facilityService.updateFacility(facilityDTO, this.facilityToEdit.id).subscribe(response =>{
        if(response){
          if(response.status == 200){
            this.openSnackBar('Serviciul a fost actualizat cu succes','');
          }
          else{
            this.openSnackBar('A aparut o erorare cu codul '+response.status,'');
          }
          this.canEditFacility = false;
          this.editFacilityForm.reset();
        }
      })
    }
    else{
      let formData = new FormData();
      formData.append("image", this.newFile);
      this.facilityService.saveImage(formData).subscribe(response =>{
        if(response){
          this.facilityService.updateFacility(facilityDTO,this.facilityToEdit.id).subscribe(response =>{
            if(response){
              if(response.status == 200){
                this.openSnackBar('Serviciul a fost actualizat cu succes','');
              }
              else{
                this.openSnackBar('A aparut o erorare cu codul '+response.status,'');
              }
              this.canEditFacility = false;
              this.editFacilityForm.reset();
            }
          })
        }
      })
    }
  }

  deleteFacility(facilityId, i){
    let snackBarRef = this.snackBar.open('Sunteti sigur ca vreti sa stergeti serviciul?','De acord',{duration: 2000, panelClass: ['blue-snackbar']});
    snackBarRef.onAction().subscribe(() =>{
      this.facilityService.deleteFacility(facilityId).subscribe(response =>{
        if(response){
          if(response.status == 200){
            this.openSnackBar('Serviciul a fost eliminat cu succes','');
            this.facilities.splice(i,1);
          }
        }
      })
    })
  }

  openSaveSpeciality(){
    this.canOpenSaveSpeciality = true;
  }
  cancelSaveSpecialityForm(){
    this.canOpenSaveSpeciality = false;
    this.saveSpecialityForm.reset();
  }

  saveSpecialization(canOpen){
    let speciality = {
      name:""
    };
    speciality.name = this.saveSpecialityForm.controls.nameControl.value;
    this.specializationService.saveSpecialization(speciality).subscribe(response =>{
      if(response){
        if(response.status == 200){
          this.openSnackBar('Specializarea a fost introdusa cu succes','');
        }
        else{
          this.openSnackBar('A aparut o eroare cu codul: '+response.status,'');
        }
        this.canOpenSaveSpeciality = canOpen;
        this.saveSpecialityForm.reset();
      }
    })
  }

  deleteSpecialization(speciality, i){
    let snackBarRef = this.snackBar.open('Stergerea unei specializari va avea ca efect stergerea tuturor medicilor afiliati acesteia.Sunteti sigur?','De acord',{duration: 6000, panelClass: ['blue-snackbar']});
    snackBarRef.onAction().subscribe(() =>{
      console.log(speciality.id);
      this.specializationService.deleteSpecialization(speciality).subscribe(response =>{
        if(response){
          if(response.status == 200){
            this.openSnackBar('Specializarea si medicii afiliati acesteia au fost eliminati cu succes','');
            this.specs.splice(i,1);
          }
        }
      })
    })
  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
      direction: 'ltr',
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    });

  }


}
