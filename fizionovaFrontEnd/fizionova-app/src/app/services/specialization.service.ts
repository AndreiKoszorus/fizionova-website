import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpecializationService {

  specializationURL = 'https://localhost:8443/specialization';

  constructor( private httpClient: HttpClient) { }

  getSpecializations():Observable<any>{
    return this.httpClient.get(this.specializationURL + "/get_specializations",{observe:'response'});
  }

  saveSpecialization(speciality):Observable<any>{
    return this.httpClient.post(this.specializationURL + '/save_specialization',speciality,{observe:'response'});
  }

  deleteSpecialization(speciality):Observable<any>{
    return this.httpClient.delete(this.specializationURL + '/delete_specialization/'+speciality.id,{observe:'response'});
  }


}
