import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FacilityService {

  facilityURL = 'https://localhost:8443/facilities/';
  constructor(private httpClient:HttpClient) { }

  saveImage(image):Observable<any>{
    return this.httpClient.post(this.facilityURL+'save_image',image);
  }

  saveFacility(facilityDTO):Observable<any>{
    return this.httpClient.post(this.facilityURL+'save_facility',facilityDTO, {observe:'response'})
  }

  getAllFacilities():Observable<any>{
    return this.httpClient.get(this.facilityURL + 'get_facilities',{observe:'response'})
  }

  updateFacility(facilityDTO, facilityId):Observable<any>{
    return this.httpClient.put(this.facilityURL + 'update_facility/'+facilityId,facilityDTO, {observe:'response'});
  }

  deleteFacility(facilityId):Observable<any>{
    return this.httpClient.delete(this.facilityURL+'delete_facility/'+facilityId,{observe:'response'});
  }
}
