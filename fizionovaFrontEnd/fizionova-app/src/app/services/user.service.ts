import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {FacilityDTO} from '../dto/facility-dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userURL = 'https://localhost:8443/user';
  constructor(private httpClient: HttpClient ) { }

  getUsers():Observable<any>{
    return this.httpClient.get(this.userURL + '/get_users');
  }

  updateUser(username,userDTO):Observable<any>{
    return this.httpClient.put(this.userURL + '/update_user/'+username,userDTO,{observe:'response'});
  }

  deleteUser(username):Observable<any>{
    return this.httpClient.delete(this.userURL + '/delete_user/'+username,{observe:'response'});
  }

  insertUser(user):Observable<any>{
    return this.httpClient.post(this.userURL + '/save_user',user,{observe:'response'});
  }

}
