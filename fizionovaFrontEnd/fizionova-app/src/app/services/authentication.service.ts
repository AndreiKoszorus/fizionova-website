import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  loginUrl = 'https://localhost:8443/login';

  constructor(private httpClient: HttpClient) { }

  login(username:string, password:string):Observable<any>{
    const btoa = window.btoa(username + ':' + password);
    return this.httpClient.post<any>(this.loginUrl + '/login', null, {
      headers: {
        Authorization: `Basic ${btoa}`
      },
      withCredentials: true
    });
  }
}
