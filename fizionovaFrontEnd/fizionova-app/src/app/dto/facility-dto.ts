

export class FacilityDTO {
  private price:number;
  private title:string;

  private description:string;

  constructor(price:number, title:string, description:string){
    this.price = price;
    this.title = title;
    this.description = description;
  }
}
