import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from '../login/login.component'
import {AdminComponent} from '../admin/admin.component';
import {AuthGuard} from '../guard/auth.guard';
import {AppComponent} from '../main-page/app.component';
import {UserComponent} from '../user/user.component';
import {UserGuard} from '../guard/user.guard';
import {NotFoundComponent} from '../not-found/not-found.component';



const appRoutes:Routes = [
  {path:'',component:AppComponent},
  {path: 'login', component: LoginComponent},
  {path:'user', component:UserComponent, canActivate:[UserGuard]},
  {path: 'admin',component:AdminComponent, canActivate:[AuthGuard]},
  {path:'**',redirectTo:'/404'},
  {path:'404', component:NotFoundComponent}
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
