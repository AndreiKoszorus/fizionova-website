import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthenticationService} from '../services/authentication.service';
import {PersistenceService} from 'angular-persistence';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router:Router){}

  canActivate(): boolean {
    const currentUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    if (!currentUser) {
      this.router.navigate(['']);
      return false;
    }
    else if(!currentUser.authdata){
      this.router.navigate(['']);
      return false;
    }
    else if(currentUser.role.role != "ADMIN"){
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
  
}
