import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate  {
  constructor(private router:Router){}

  canActivate(): boolean {
    const currentUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    if (!currentUser) {
      this.router.navigate(['']);
      return false;
    }
    else if(!currentUser.authdata){
      this.router.navigate(['']);
      return false;
    }
    else if(currentUser.role.role != "USER"){
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}
