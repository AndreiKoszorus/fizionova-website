import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {AppService} from './app.service';
import {Moment} from 'moment';
import {MatCalendar, MatCalendarCellCssClasses, MatHorizontalStepper, MatSnackBar} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DoctorServiceService} from '../services/doctor-service.service';
import {SpecializationService} from '../services/specialization.service';
import {NavigationEnd, Router} from '@angular/router';
import {FacilityService} from '../services/facility.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

declare let EventSource: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
export class AppComponent implements OnInit {

  eventsArray = [];
  fullDates = [];
  // can load the calendar variable
  canLoadCalendar: boolean = false;
  verticalStepper:boolean = false;
  tomorrow = new Date();
  lastDate = new Date();
  newEvents = [];
  // all events must be sorted on an ascending order so we can determine based on isFullDay function if a day si full or not
  sortedEvents = [];
  //currentlySelectedMonth
  currentMonth;
  // available hours displayed
  hours = [];
  // currently selected date on calander. It has different borded color.
  selectedDate: Moment;
  step3Completed = false;
  // time of a consultation
  consultationTime = 20;
  step4Completed = false;
  programmedDate;
  startHour;
  endHour;
  currentStep;
  weekendFilter;
  filterByMonth;
  filterByName;
  loading: boolean = false;
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  @ViewChild('calendar', {static: false}) calendar: MatCalendar<Moment>;
  @ViewChild('stepper', {static:false}) stepper: MatHorizontalStepper;

  specialties = [];
  facilities = [];
  specialtyControl = new FormControl('', Validators.required);
  hoursControl = new FormControl('', Validators.required);
  doctorControl = new FormControl('', Validators.required);
  doctors = [];
  allDoctors = [];
  filterBySpecialty = [];
  transmittedEvent ={
    startEvent:Date,
    endEvent:Date,
    description:"",
    calendarName:"",
    status:"",
    eventId:"",
    allDay: false,
    doctorName:""
  };

  startDate;
  endDate;
  url = " ";

  eventsSet:Set<any>;
  canLoadStepper:boolean = false;
  innerWidth:any;
  constructor(private appService: AppService, private sanitizer:DomSanitizer,private snackBar: MatSnackBar, private formBuilder: FormBuilder, private doctorService:DoctorServiceService, private specializationService:SpecializationService, private router:Router, private facilityService:FacilityService) {
    this.eventsSet = new Set<any>();
  }

  informationControlGroup: FormGroup;

  ngOnInit(): void {

    this.innerWidth = window.innerWidth;
    if(this.innerWidth < 800){
      this.verticalStepper = true
    }
    else{
      this.verticalStepper = false;
    }

    console.log('Screen width is:' + this.innerWidth);

    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.url = e.url.slice(1);
        console.log(this.url[0]);
      }
    });
    this.tomorrow.setDate(this.tomorrow.getDate() + 1);
    this.weekendFilter = (moment: Moment) => moment.toDate().getDay() >= 1 && moment.toDate().getDay() <= 5;
    //validators for information form
    this.informationControlGroup = this.formBuilder.group({
      firstNameControl: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]+')])],
      secondNameControl: ['', Validators.compose([Validators.pattern('[a-zA-Z]+'), Validators.required])],
      phoneControl: ['', Validators.compose([Validators.required, Validators.pattern('^(\\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\\s|\\.|\\-)?([0-9]{3}(\\s|\\.|\\-|)){2}$')])],
    });

    this.specializationService.getSpecializations().subscribe(response =>{
      if(response){
        if(response.status == 200){
        for(let i=0; i< response.body.length;i++) {
          this.specialties.push(response.body[i].name);
          }
        }
        else{
          this.openSnackBar("A apărut o erorare. Va rugam reîncărcați pagina",'');
        }
      }
    });

    this.doctorService.getDoctors().subscribe(response =>{
      if(response){
        if(response.status == 200){
          for(let i=0; i< response.body.length;i++){
            this.allDoctors.push(response.body[i]);
          }
        }
        else{
          this.openSnackBar("A apărut o erorare. Va rugam reîncărcați pagina",'');
        }
      }
    });

    this.facilityService.getAllFacilities().subscribe(response =>{
      if(response){
        if(response.status == 200){
          this.facilities = response.body;

        }
        else{
          this.openSnackBar("A apărut o erorare cu codul"+response.status,'');
        }

      }
    });

    // get all events from calendar to get client in sync
    this.appService.getInSync().subscribe(response => {
      if (response) {
        if (response.status == 200) {
          if(response.body!= "") {
            this.eventsArray = response.body;
            this.sortedEvents = this.eventsArray.sort(function (a, b) {
              if (new Date(a.startEvent).getTime() < new Date(b.startEvent).getTime()) return -1;
              if (new Date(a.startEvent).getTime() > new Date(b.startEvent).getTime()) return 1;

              return 0;
            });
            let lastElement = this.sortedEvents[this.sortedEvents.length - 1];
            if (lastElement.description == 'End' || lastElement.allDay) {
              this.lastDate = new Date(lastElement.startEvent);
            }
            // we must load stepper only when the event array is fetched and sorted
            this.canLoadStepper = true;
          }
        }
        else{
          this.openSnackBar("A apărut o erorare. Va rugam reîncărcați pagina",'');
        }
      }
    });
// opens websoscket connection with the server having bidirectional communication without the need of http protocol.Data are transmitted as stream of bytes.
    this.connect();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    if(this.innerWidth < 800){
      this.verticalStepper = true;
    }
    else{
      this.verticalStepper = false;
    }
    console.log('Windows size'+this.innerWidth);
  }
  transformImage(img):SafeResourceUrl{
    return this.sanitizer.bypassSecurityTrustResourceUrl('data:image/*;base64,'+img);
  }
// websocket connection with the backend. This function receives all new events occured during the client usage of the site. Bcs of the websocket the communication is instant and is not based on HTTP protocol
  // in this way the events and the hours can be computed dynamically without the need to refresh the page
  connect(): void {
    let source = new EventSource('https://localhost:8443/web-socket/stream');
    source.addEventListener('message', message => {

      this.newEvents = JSON.parse(message.data);
      console.log( "New events: ", this.newEvents);
      //delete cancelled events from sortedArray
      this.deleteCancelledEvents(this.newEvents);
      let filterCancelled = this.newEvents.filter(event => event.status != 'cancelled');
      //insert new events and resort the array
      let newArray = this.sortedEvents.concat(filterCancelled);
// here we store all events from all users and we sort them in ascending order
      this.sortedEvents = newArray.sort(function (a, b) {
        if (new Date(a.startEvent).getTime() < new Date(b.startEvent).getTime()) return -1;
        if (new Date(a.startEvent).getTime() > new Date(b.startEvent).getTime()) return 1;

        return 0;
      });
      console.log("Sorted array: ", this.sortedEvents);
// if the month is not undefined, so the calendar is already loaded we refilter everything so we can update the events that changed
      if (this.currentMonth != undefined) {
        console.log("enter");
        this.filterBySpecialty = this.sortedEvents.filter(date => date.specialization == this.specialtyControl.value);
        this.filterByName = this.filterBySpecialty.filter(event=> event.doctorName == this.doctorControl.value);
        this.filterByMonth = this.filterByName.filter(date => new Date(date.startEvent).getMonth() == this.currentMonth);

        if (this.filterByName.length > 0) {
          let lastElement = this.filterByName[this.filterByName.length - 1];
          if (lastElement.description == 'End' || lastElement.allDay) {
            this.lastDate = new Date(lastElement.startEvent);
          }
        }
        else {
          this.lastDate = this.tomorrow;
        }
        if(this.selectedDate != undefined) {
          if (this.selectedDate.toDate().getTime() > this.lastDate.getTime()) {
            this.selectedDate = undefined;
            this.hoursControl.setValue(undefined);
          }
        }
        // if the client is on the 4th step, to choose an available hour, and a hour is occupied meanwhile that hour will pe dynamically erased from choices
        if (this.currentStep != undefined) {
          if (this.currentStep.selectedIndex == 3) {
            if(this.selectedDate != undefined) {
              let dayHours = this.filterByMonth.filter(event => new Date(event.startEvent).getDate() == this.selectedDate.toDate().getDate());
              this.computeAvailableHours(dayHours);
            }
            else{
              this.openSnackBar('Data aleasa tocmai a devenit indisponiblia. Va rugam reveniti la pasul anterior pnetru a selecta alta data','');
            }
          }
        }
        //if the client is on the calendar view and a day becomes full that day will be dynamically changed to red background becoming unavailable
        this.updateDatesDynamically(this.filterByMonth);
      }
    });

  }

// function to delete from sortedArray ,which stores all the events from calendar in ascending order based on date, the cancelled events
  deleteCancelledEvents(newEvents) {
    let newEventsLength = newEvents.length;

    for (let i = 0; i < newEventsLength; i++) {
      if (newEvents[i].status == 'cancelled') {
        this.sortedEvents = this.sortedEvents.filter(event => event.eventId != newEvents[i].eventId);
      }
    }
  }

// algorithm to find if a date is full or not. Uses as reference as start and end of a day the Start and End descriptions
  isFullDay(events): any {
    const eventsLength = events.length;
    let freeDay: boolean = false;
    let fullDays = [];

    for (let i = 0; i < eventsLength; i++) {
      if (('End' == events[i].description && !freeDay) || events[i].allDay) {
        let date = new Date(events[i].startEvent);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);

        if (!fullDays.includes(date)) {
          fullDays.push(date);
        }
      }
      else if ('End' == events[i].description && freeDay) {
        freeDay = false;
      }
      else if (i + 1 != eventsLength) {
        if (!this.compareDates(events[i].endEvent, events[i + 1].startEvent) && !('Start' == events[i + 1].description)) {
          freeDay = true;
        }
      }
    }
    return fullDays;
  }

// functions to compare dates
  compareDates(a, b): boolean {
    let dateA = new Date(a);
    let dateB = new Date(b);
    return (dateA.getTime() == dateB.getTime());
  }

  sameDate(date1: Date, date2: Date): boolean {
    return ((date1.getDate() == date2.getDate()) && (date1.getMonth() == date2.getMonth()) && (date1.getFullYear() == date2.getFullYear()));
  }

//checks if a date is present in that array
  checkPresenceOfDate(fullDates, date): boolean {
    const dateLength = fullDates.length;

    for (let i = 0; i < dateLength; i++) {
      if (this.sameDate(fullDates[i], date)) {
        return true;
      }
    }
    return false;
  }


  // function to generate css classes for mat calendar cells.This function is called anytime the calendar view changes so we need to know available or unavailable dates in order to give them green or red background
  dateClass() {
    return (moment: Moment): MatCalendarCellCssClasses => {

      if (moment.toDate().getDate() == 1) {

        this.currentMonth = moment.toDate().getMonth();
        this.filterBySpecialty = this.sortedEvents.filter(event => event.specialization == this.specialtyControl.value);
        this.filterByName = this.filterBySpecialty.filter(event => event.doctorName == this.doctorControl.value);
        this.filterByMonth = this.filterByName.filter(event => new Date(event.startEvent).getMonth() == moment.toDate().getMonth());
        if (this.filterByName.length > 0) {
          let lastElement = this.filterByName[this.filterByName.length - 1];
          if (lastElement.description == 'End' || lastElement.allDay) {
            this.lastDate = new Date(lastElement.startEvent);

          }
        }
        else {
          this.lastDate = this.tomorrow;
        }
        this.fullDates = this.isFullDay(this.filterByMonth);
      }

      if (moment.toDate().setHours(0, 0, 0, 0) < this.tomorrow.setHours(0, 0, 0, 0,) || moment.toDate().setHours(0, 0, 0, 0) > this.lastDate.setHours(0, 0, 0, 0) || moment.toDate().getDay() == 6 || moment.toDate().getDay() == 0) {
        return;
      }
      else if (this.checkPresenceOfDate(this.fullDates, moment.toDate())) {
        if (this.selectedDate != undefined) {
          if (this.compareDates(moment.toDate(), this.selectedDate.toDate())) {
            this.selectedDate = undefined;
          }
        }
        return 'blocked-date div';
      }
      else if (this.checkPresenceOfDate(this.filterByName.map(e => new Date(e.startEvent)), moment.toDate())) {
        return 'free-date div ';
      }
      else {
        return 'blocked-date div';
      }
    };
  }

// checks if a day is present in an array of dates
  checkPresenceOfDay(day, dates): boolean {
    let len = dates.length;

    for (let i = 0; i < len; i++) {
      if (dates[i].getDate() == day) {
        return true;
      }
    }
    return false;
  }

  findMonth(stringMonth) {
    for (let i = 0; i < this.months.length; i++) {
      if (this.months[i] == stringMonth) {
        return i;
      }
    }

  }

  //update dates on calendar when dynamically when new events are inserted. We take DOM elements that are calendar cell and check if those day changed or not. If they changed we give the html element new css classes
  // so they become dynamically either available or unavailable.
  updateDatesDynamically(datesToUpdate) {

    let elements = document.getElementsByClassName('mat-calendar-body-cell');
    let fullDates = this.isFullDay(datesToUpdate);
    // if the currently selected date becomes full it must be unselected and to block the user to go to the next step
    if (this.selectedDate != undefined) {
      if (this.checkPresenceOfDay(this.selectedDate.toDate().getDate(), fullDates)) {
        this.selectedDate = undefined;
        this.step3Completed = false;
      }
    }
    for (let i = 0; i < elements.length; i++) {
      let currentDateString = elements[i].getAttribute('aria-label');
      let dateParts = currentDateString.split(' ');
      let dateMonth = this.findMonth(dateParts[0]);
      let dayParts = dateParts[1].split(',');
      let dateDay = parseInt(dayParts[0]);
      let dateYear = parseInt(dateParts[2]);
      let date = new Date();
      date.setHours(0, 0, 0, 0,);
      date.setDate(dateDay);
      date.setMonth(dateMonth);
      date.setFullYear(dateYear);
      if (date >= this.tomorrow && date <= this.lastDate) {
        let fullDay = this.checkPresenceOfDate(fullDates, date);
        if (fullDay) {
          elements[i].className = 'mat-calendar-body-cell blocked-date div mat-calendar-body-active ng-star-inserted';
        }
        else if (this.checkPresenceOfDate(this.filterByName.map(event => new Date(event.startEvent)),date) && !fullDay) {
          elements[i].className = 'mat-calendar-body-cell free-date div mat-calendar-body-active ng-star-inserted';
        }
        else if(date.getDay() == 6 || date.getDay() == 0) {
          elements[i].className = 'mat-calendar-body-cell mat-calendar-body-disabled ng-star-inserted';
        }
        else{
          if(this.selectedDate.toDate().getTime() == date.getTime()){
            this.selectedDate = undefined;
          }
          elements[i].className = 'mat-calendar-body-cell blocked-date div mat-calendar-body-active ng-star-inserted';
        }
      }
      if(date > this.lastDate && date.getDay() != 0 && date.getDay() != 6){
        elements[i].className = 'mat-calendar-body-cell ng-star-inserted';
      }

    }

  }

  selectDateCalendar(moment: Moment) {
    let blockedDates = document.getElementsByClassName('blocked-date');
    let freeDates = document.getElementsByClassName('free-date');
    let dateSelected = moment.toDate();

    for (let i = 0; i < blockedDates.length; i++) {
      let elementContent = parseInt(blockedDates[i].children[0].innerHTML);
      if (elementContent == dateSelected.getDate()) {
        this.openSnackBar('Nu puteti selecta o data indisponibila', '');
        break;
      }
    }

    for (let i = 0; i < freeDates.length; i++) {
      let elementContent = parseInt(freeDates[i].children[0].innerHTML);
      if (elementContent == dateSelected.getDate()) {
        this.selectedDate = moment;
        this.step3Completed = true;
        console.log(this.filterByMonth);
        let dayEvents = this.filterByMonth.filter(event => new Date(event.startEvent).getDate() == dateSelected.getDate());
        this.computeAvailableHours(dayEvents);
        break;
      }
    }

  }

  computeAvailableHours(dayEvents) {
    let dayHoursLen = dayEvents.length;
    this.hours = [];

    for (let i = 0; i < dayHoursLen; i++) {
      if ('End' != dayEvents[i].description) {
        let start: Date = new Date(dayEvents[i].endEvent);
        let end: Date = new Date(dayEvents[i + 1].startEvent);
        if (start != end) {
          let timeInterval = ((end.getTime() - start.getTime()) / (1000 * 60)) / this.consultationTime;
          let freeHour = start;
          for (let j = 0; j < timeInterval; j++) {
            let time = freeHour.getHours() + ' : ' + freeHour.getMinutes() + ((freeHour.getMinutes() == 0) ? '0' : '');
            this.hours.push(time);
            freeHour = new Date(freeHour.setMinutes(freeHour.getMinutes() + this.consultationTime));
          }
        }
      }
    }
  }

// control of the stepper bar for online programming
  stepperChanged(event) {
    this.currentStep = event;
    if (event.selectedIndex == 0) {
      this.selectedDate = undefined;
      this.canLoadCalendar = false;
      this.step3Completed = false;
      this.doctorControl.setValue(undefined);
      this.hoursControl.setValue(undefined);
    }
    if (event.selectedIndex == 2 && event.previouslySelectedIndex == 1) {
      this.selectedDate = undefined;
      this.canLoadCalendar = true;
    }
    if (event.selectedIndex == 2 && event.previouslySelectedIndex == 4) {
      this.hoursControl.setValue(undefined);
    }

    if (event.selectedIndex == 1 && event.previouslySelectedIndex == 2) {
      this.canLoadCalendar = false;
      this.step3Completed = false;
    }

    if(event.selectedIndex == 2 && event.previouslySelectedIndex == 3){
      this.updateDatesDynamically(this.filterByName);
      this.hoursControl.setValue(undefined);
    }

    if(event.selectedIndex == 1 && event.previouslySelectedIndex == 4){
        this.hoursControl.setValue(undefined);
        this.step3Completed = false;
    }

    if(event.selectedIndex == 1 && event.previouslySelectedIndex == 3){
      this.canLoadCalendar = false;
      this.hoursControl.setValue(undefined);
      this.step3Completed = false;
    }

    if(event.selectedIndex == 1){
      this.doctors = this.allDoctors.filter(doctor => doctor.specialization.name == this.specialtyControl.value);
    }

    if (event.selectedIndex == 4) {
      let startStringHours = this.hoursControl.value.split(' : ', 2);
      let startHour = parseInt(startStringHours[0]);
      let startMinutes = parseInt(startStringHours[1]);
      this.startDate = this.selectedDate.toDate();
      this.endDate = this.selectedDate.toDate();
      this.startDate.setHours(startHour, startMinutes, 0, 0);
      this.endDate.setHours(startHour, startMinutes, 0, 0);
      this.endDate.setMinutes(this.endDate.getMinutes() + 20);
      this.programmedDate = '' + this.startDate.getDate() + ' / ' + ((this.startDate.getMonth() + 1 > 9) ? '' : '0') + (this.startDate.getMonth() + 1) + ' / ' + this.startDate.getFullYear();
      this.startHour = '' + this.startDate.getHours() + ' : ' + this.startDate.getMinutes() + ((this.startDate.getMinutes() == 0) ? '0' : '');
      this.endHour = '' + this.endDate.getHours() + ' : ' + this.endDate.getMinutes() + ((this.endDate.getMinutes() == 0) ? '0' : '');
    }
  }

  onFormSubmit(){

    this.loading = true;
    this.transmittedEvent.startEvent = this.startDate;
    this.transmittedEvent.endEvent = this.endDate;
    this.transmittedEvent.description = "Nume: "+this.informationControlGroup.controls.firstNameControl.value + " "+ "Prenume: "+this.informationControlGroup.controls.secondNameControl.value + " " + "Numar de telefon: " + this.informationControlGroup.controls.phoneControl.value;
    this.transmittedEvent.calendarName = this.specialtyControl.value;
    this.transmittedEvent.status = "";
    this.transmittedEvent.eventId = "";
    this.transmittedEvent.allDay = false;
    this.transmittedEvent.doctorName = this.doctorControl.value;
    this.appService.insertEvent(this.transmittedEvent).subscribe(response=>{
      if(response){
        if(response.status == 200){
          if(response.body) {
            this.openSnackBar('Programarea dumneavoastră a fost introdusă cu succes', '');
          }
          else {
            this.openSnackBar('Data si ora pe care ati selectat-o tocmai s-au ocupat. Va rugam alegeti alta data.', '');

          }
        }
        else{
          this.openSnackBar('A apărut o eroare. Vă rugăm reîncercați.','');
        }
        this.stepper.reset();
        this.loading = false;

      }
    });
  }

  cancelScheduleForm(){
    let snackBarRef = this.snackBar.open('Doriti sa anulati programarea?','Da',{duration: 4000, panelClass: ['blue-snackbar']});
    snackBarRef.onAction().subscribe(()=>{
      this.stepper.reset();
    })
  }

  // bar to inform user about operations
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
      direction: 'ltr',
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    });
  }

}
