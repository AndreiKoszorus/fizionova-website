import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import {AppService} from './app.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FullCalendarModule } from '@fullcalendar/angular';
import {
  MatButtonModule, MatCardModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule, MatProgressSpinnerModule,
  MatSelectModule, MatSnackBarModule,
  MatStepperModule,
} from '@angular/material';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SpecializationService} from '../services/specialization.service';
import {DoctorServiceService} from '../services/doctor-service.service';
import { LoginComponent } from '../login/login.component';

import { AppRoutingModule } from '../routes/app-routing.module';
import {RouterModule} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import { AdminComponent } from '../admin/admin.component';
import {InterceptorService} from '../interceptor/interceptor.service';
import {PersistenceModule} from 'angular-persistence';
import {UserService} from '../services/user.service';
import {FacilityService} from '../services/facility.service';
import {CookieService} from 'ngx-cookie-service';
import { UserComponent } from '../user/user.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    UserComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FullCalendarModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatStepperModule,
    MatButtonModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    PersistenceModule,
    MatCardModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    AppRoutingModule
  ],
  providers: [AppService, HttpClientModule,SpecializationService,DoctorServiceService, AuthenticationService, UserService, FacilityService,CookieService,{ provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
