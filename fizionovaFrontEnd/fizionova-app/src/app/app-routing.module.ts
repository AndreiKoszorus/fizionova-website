import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from '../login/login.component'
import {AdminComponent} from './admin/admin.component';
import {AuthGuard} from './guard/auth.guard';
import {AppComponent} from './app.component';



const appRoutes:Routes = [
  {path: '', component: AppComponent},
  {path: 'login', component: LoginComponent},
  {path: 'admin',component:AdminComponent, canActivate:[AuthGuard]}
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
