import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SpecializationService} from '../services/specialization.service';
import {MatSnackBar} from '@angular/material';
import {UserService} from '../services/user.service';
import {AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  loggedUser;
  username:string = '';
  userInfoForm:FormGroup;
  specs = [];
  roles = ["ADMIN","USER"];
  constructor(private formBuilder:FormBuilder, private specializationService: SpecializationService, private snackBar:MatSnackBar, private userService:UserService, private authenticationService:AuthenticationService) {
    this.userInfoForm = this.formBuilder.group({
        usernameControl: [{disabled: true, value:"Username"}],
        calendarIDControl:['',Validators.required],
        specializationControl: ['',Validators.required],
        roleControl:[{disabled: true, value:"Rol"}]
      }
    );
  }

  ngOnInit() {
    this.loggedUser = JSON.parse(sessionStorage.getItem("loggedUser"));
    this.username = this.loggedUser.username;


    this.specializationService.getSpecializations().subscribe(response =>{
      if(response){
        if(response.status == 200){
          for(let i=0; i< response.body.length;i++) {
            this.specs.push(response.body[i]);
          }
        }
        else{
          this.openSnackBar("A apărut o erorare cu codul"+response.status,'');
        }
      }
    });

    this.userInfoForm.controls.usernameControl.setValue(this.loggedUser.username);
    this.userInfoForm.controls.calendarIDControl.setValue(this.loggedUser.calendarId);
    this.userInfoForm.controls.specializationControl.setValue(this.loggedUser.specialization.name);
    this.userInfoForm.controls.roleControl.setValue(this.loggedUser.role.role);

  }

  saveModifications(){
    let snackBarRef = this.snackBar.open('Salvati modificarile?','Da',{duration: 3000, panelClass: ['blue-snackbar']});
    snackBarRef.onAction().subscribe(()=>{
      let userDTO = {
        username:"",
        specialization:"",
        calendarId:"",
        role:""
      };
      userDTO.username = this.userInfoForm.controls.usernameControl.value;
      userDTO.specialization = this.userInfoForm.controls.specializationControl.value;
      userDTO.calendarId = this.userInfoForm.controls.calendarIDControl.value;
      userDTO.role = this.userInfoForm.controls.roleControl.value;
        this.userService.updateUser(this.loggedUser.username, userDTO).subscribe(response =>{
          if(response){
            if(response.status == 200){
              this.openSnackBar('Modificarile au fost facute cu succes','');
            }
          }
        })
    })
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
      direction: 'ltr',
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    });

  }

}
