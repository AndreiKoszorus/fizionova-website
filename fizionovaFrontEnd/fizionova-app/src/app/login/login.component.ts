import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {AppService} from '../main-page/app.service';
import {PersistenceService} from 'angular-persistence';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private route: Router, private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usernameControl: ['', Validators.required],
      passwordControl: ['', Validators.required]
    });
  }

  onFormSubmit() {

    let username = this.loginForm.controls.usernameControl.value;
    let password = this.loginForm.controls.passwordControl.value;
    this.authenticationService.login(username, password).subscribe(response => {
        if (response) {
          response.authdata = window.btoa(username + ':' + password);
          sessionStorage.setItem('loggedUser', JSON.stringify(response));
         if(response.role.role == "ADMIN"){
           this.route.navigate(['admin']);
         }
         else if(response.role.role == "USER"){
           this.route.navigate(['user']);
         }

        }
      },
      error1 => {
        alert('Combinatie de username si parola gresite')
      });
  }

}
