import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpecializationService {

  specializationURL = 'http://localhost:8080/specialization';

  constructor( private httpClient: HttpClient) { }

  getSpecializations():Observable<any>{
    return this.httpClient.get(this.specializationURL + "/get_specializations",{observe:'response'});
  }
}
