import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {AppService} from '../app/app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private route: Router, private authenticationService: AuthenticationService, private appService: AppService) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usernameControl: ['', Validators.required],
      passwordControl: ['', Validators.required]
    });
  }

  onFormSubmit() {

    let username = this.loginForm.controls.usernameControl.value;
    let password = this.loginForm.controls.passwordControl.value;
    this.authenticationService.login(username, password).subscribe(response => {
        if (response) {

          response.authdata = window.btoa(username + ':' + password);
          sessionStorage.setItem('loggedUser', JSON.stringify(response));
          this.authenticationService.cacheLoggedUser(JSON.stringify(response));
          this.route.navigate(['admin']);
        }
      },
      error1 => {
        alert('Combinatie de username si parola gresite')
      });
  }

  test() {
    console.log("dada");
    this.appService.test().subscribe(response => {
      if (response) {
        console.log(response);
      }
      else {
        console.log("error")
      }
    });

  }
}
