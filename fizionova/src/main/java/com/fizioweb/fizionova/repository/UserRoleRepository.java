package com.fizioweb.fizionova.repository;

import com.fizioweb.fizionova.models.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

    UserRole findByRole (String role);
}
