package com.fizioweb.fizionova.repository;

import com.fizioweb.fizionova.models.Specialization;
import com.fizioweb.fizionova.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.IOException;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    void deleteByUsername(String username);
    User findByUsername(String username);
    void deleteAllBySpecialization(Specialization specialization);
}
