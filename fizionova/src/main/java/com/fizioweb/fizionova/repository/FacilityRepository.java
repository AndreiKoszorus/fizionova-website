package com.fizioweb.fizionova.repository;

import com.fizioweb.fizionova.models.Facility;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacilityRepository extends JpaRepository<Facility,Integer> {

    Facility findFacilityById(int facilityId);
}
