package com.fizioweb.fizionova.repository;

import com.fizioweb.fizionova.models.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpecializationRepository extends JpaRepository<Specialization, Integer> {

    Specialization findByName(String name);
    List<Specialization> findAll();
    Specialization findById(int id);
}
