package com.fizioweb.fizionova.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.sql.DataSource;
import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/facilities//get_facilities").permitAll()
                .antMatchers("/calendar/**").permitAll();

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/user/save_user").hasAuthority("ADMIN")
                .antMatchers("/user/delete_user/**").hasAuthority("ADMIN")
                .antMatchers("/user/update_user/**").permitAll()
                .antMatchers("/facilities/save_image").hasAuthority("ADMIN")
                .antMatchers("/facilities/save_facility").hasAuthority("ADMIN")
                .antMatchers("/facilities/get_facility/**").hasAuthority("ADMIN")
                .antMatchers("/facilities/delete_facility/**").hasAuthority("ADMIN")
                .antMatchers("/facilities/update_facility/**").hasAuthority("ADMIN")
                .antMatchers("/user/get_user/**").hasAuthority("USER")
                .antMatchers("/login").permitAll()
                .and().httpBasic()
                .authenticationEntryPoint(authenticationEntryPoint);

        http.cors();
    }

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }


    @Autowired
    private DataSource dataSource;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select username, password, active AS enabled from users where username=?")
                .authoritiesByUsernameQuery("select a.username AS username, b.role AS authority from users a JOIN roles b on a.role_id = b.id where a.username=?")
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("HEAD",
                "GET", "POST", "PUT", "DELETE", "PATCH"));
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.setAllowCredentials(true);
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}