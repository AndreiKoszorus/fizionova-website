package com.fizioweb.fizionova.controllers;

import com.fizioweb.fizionova.models.Specialization;
import com.fizioweb.fizionova.services.SpecializationService;
import com.fizioweb.fizionova.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/specialization")
@CrossOrigin
public class SpecializationsControlller {

    @Autowired
    private SpecializationService specializationService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/get_specializations", produces = "application/json")
    public List<Specialization> allSpecializations(){
        return specializationService.findAll();
    }

    @PostMapping(value = "/save_specialization", produces = "application/json")
    public void saveSpecialization(@RequestBody Specialization specialization){
            specializationService.insertSpecialization(specialization);
    }

    @DeleteMapping(value = "/delete_specialization/{specialityId}",produces = "application/json")
    public void deleteSpecialization(@PathVariable("specialityId") int specializationId){
        Specialization specialization = specializationService.findSpecializationById(specializationId);
        if(specialization != null){
            userService.deleteAllBySpecialization(specialization);
            specializationService.deleteSpecialization(specialization);
        }
    }

}
