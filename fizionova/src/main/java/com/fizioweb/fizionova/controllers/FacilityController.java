package com.fizioweb.fizionova.controllers;


import com.fizioweb.fizionova.DTO.FacilityDTO;
import com.fizioweb.fizionova.models.Facility;
import com.fizioweb.fizionova.repository.FacilityRepository;
import com.fizioweb.fizionova.services.FacilityService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/facilities")
@MultipartConfig
@CrossOrigin
public class FacilityController {

    @Autowired
    private FacilityService facilityService;

    @Autowired
    private FacilityRepository facilityRepository;

    @Autowired
    private ModelMapper modelMapper;

    private byte[] currentImageBytes;

    @PostMapping(value="/save_image",produces = "application/json")
    public boolean cacheImage(@RequestParam("image")MultipartFile image) throws IOException{
        currentImageBytes = image.getBytes();
        return true;
    }
    @PostMapping(value = "/save_facility", produces = "application/json")
    public void saveFacility( @RequestBody FacilityDTO facilityDTO) {
        Facility facility = modelMapper.map(facilityDTO,Facility.class);
        facility.setImgPath(currentImageBytes);
       facilityService.saveFacility(facility);
    }

    @GetMapping(value = "/get_facilities", produces = "application/json")
    public List<Facility> getFacilities (){
        return facilityService.getAll();
    }
    @GetMapping(value = "/get_facility/{id}", produces = "application/json")
    public Facility getFacilities (@PathVariable("id") int id){
        return facilityRepository.findFacilityById(id);
    }

    @DeleteMapping(value = "/delete_facility/{facilityId}", produces = "application/json")
    public void deleteFacility (@PathVariable("facilityId") int facilityId){
        facilityService.deleteFacility(facilityId);
    }

    @PutMapping(value = "/update_facility/{facilityId}", produces = "application/json")
    public void updateFacility (@PathVariable("facilityId") int facilityId, @RequestBody FacilityDTO updatedFacilityDTO){
        Facility oldFacility = facilityService.getFacilityById(facilityId);
        Facility newFacility = modelMapper.map(updatedFacilityDTO, Facility.class);
        if(currentImageBytes == null){
            newFacility.setImgPath(oldFacility.getImgPath());
        }
        else{
            newFacility.setImgPath(currentImageBytes);
        }
        facilityService.updateFacility(facilityId,newFacility);
    }
}
