package com.fizioweb.fizionova.controllers;


import com.fizioweb.fizionova.models.TransmittedEvent;
import com.fizioweb.fizionova.services.CalendarEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.fizioweb.fizionova.utils.SendEventsUtil.emitters;


@RestController
@RequestMapping(value = "/calendar")
@CrossOrigin
public class CalendarController {

    @Autowired
    private CalendarEventService calendarEventService;


    @PostMapping(value = "/insert_event", produces = "application/json")
    public boolean insertEvent(@RequestBody TransmittedEvent transmittedEvent){
        boolean canInsert = true;
        try {

            canInsert = calendarEventService.insertEvent(transmittedEvent);
        } catch (IOException ex){
            System.out.print(ex.getMessage());
        }
        return canInsert;
    }

    @GetMapping(value="/sync_events", produces = "application/json")
    public List<TransmittedEvent> transmittedEvents(){
        List<TransmittedEvent> events = new ArrayList<>();
        try {
          events = calendarEventService.getInSync();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }

        return events;
    }


}
