package com.fizioweb.fizionova.controllers;


import com.fizioweb.fizionova.DTO.UserDTO;
import com.fizioweb.fizionova.models.Specialization;
import com.fizioweb.fizionova.models.User;
import com.fizioweb.fizionova.services.UserService;
import com.fizioweb.fizionova.services.UserServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/save_user", produces = "application/json")
    public int createUser(@RequestBody User user ){
        return userService.saveUser(user);
    }

    @GetMapping(value = "/get_users",produces = "application/json")
    public List<UserDTO> getUsers(){
      return userService.getAllUsers();
    }

    @GetMapping(value="/get_user/{username}", produces = "application/json")
    public UserDTO getUserDTOByUsername(@PathVariable("username") String username){
        return userService.getUserDTOByUsername(username);
    }

    @DeleteMapping(value = "/delete_user/{username}",produces = "application/json")
    public Boolean deleteUser(@PathVariable("username") String username){
        return userService.deleteUser(username);

    }

    @PutMapping(value = "/update_user/{username}", produces = "application/json")
    public void updateUser(@PathVariable("username") String username, @RequestBody UserDTO userDTO){
        userService.updateUser(username,userDTO);
    }


}
