package com.fizioweb.fizionova.controllers;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import static com.fizioweb.fizionova.utils.SendEventsUtil.emitters;

@RestController
@RequestMapping(value = "/web-socket")
@CrossOrigin
public class WebSocketController {

    @RequestMapping(path = "/stream", method = RequestMethod.GET)
    public SseEmitter stream() {

        SseEmitter emitter = new SseEmitter();

        emitters.add(emitter);
        emitter.onCompletion(() -> emitters.remove(emitter));

        return emitter;
    }
}
