package com.fizioweb.fizionova.controllers;


import com.fizioweb.fizionova.DTO.UserDTO;
import com.fizioweb.fizionova.models.User;
import com.fizioweb.fizionova.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/login")
@CrossOrigin
public class LogInController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/login")
    public UserDTO logUserIn(Authentication authentication) {
        // Spring Security will prohibit the user from reaching this point if the credentials are incorrect
        UserDetails details = (UserDetails) authentication.getPrincipal();
        User user = userService.findUserByUsername(details.getUsername());
        return modelMapper.map(user,UserDTO.class);
    }

}
