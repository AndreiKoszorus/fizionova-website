package com.fizioweb.fizionova.utils;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class CalendarUtils {

    private static final String APPLICATION_NAME = "Fizionova";

    private static final java.io.File DATA_STORE_DIR =
            new java.io.File(System.getProperty("user.dir"), "store/calendar-sync");

    private static FileDataStoreFactory dataStoreFactory;

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static HttpTransport httpTransport;

    private static final String SERVICE_ACCOUNT_ID = "fizionova@usercredential.iam.gserviceaccount.com";

    static {
        try {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /** Creates a new Calendar client to use when making requests to the API. */
    public static Calendar createCalendarClient(List<String> scopes) throws Exception {

        Credential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(JSON_FACTORY)
                .setServiceAccountId(SERVICE_ACCOUNT_ID)
                .setServiceAccountScopes(scopes)
                .setServiceAccountPrivateKeyFromP12File( new java.io.File(System.getProperty("user.dir"), "src/main/resources/UserCredential.p12"))
                .build();
        return new Calendar.Builder(
                httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
    }

    /** Gets the datastore factory used in these samples. */
    public static DataStoreFactory getDataStoreFactory() {
        return dataStoreFactory;
    }

    /** Creates a test event. */
    public static void createEvent(Calendar client, Event event, String calendarId) throws IOException {
        client.events().insert(calendarId, event).execute();
    }

    public static Date getDateWithoutTimeUsingCalendar() {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);
        calendar.set(java.util.Calendar.MINUTE, 0);
        calendar.set(java.util.Calendar.SECOND, 0);
        calendar.set(java.util.Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }
}
