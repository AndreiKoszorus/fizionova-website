package com.fizioweb.fizionova.utils;

import com.fizioweb.fizionova.models.TransmittedEvent;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class SendEventsUtil {
    public static final List<SseEmitter> emitters = Collections.synchronizedList( new ArrayList<>());

    public static void sendSeeEventsToUi( List<TransmittedEvent> transmittedEvents){
        ArrayList<SseEmitter> cancelledEmitters = new ArrayList<>();
        System.out.println(emitters.size());
        emitters.forEach((SseEmitter emitter) -> {
            try {
                emitter.send(transmittedEvents, MediaType.APPLICATION_JSON);
            } catch (IOException e) {
                emitter.complete();
                cancelledEmitters.add(emitter);
                e.printStackTrace();
            }
        });
        emitters.removeAll(cancelledEmitters);
    }
}
