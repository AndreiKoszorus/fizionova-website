package com.fizioweb.fizionova.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.fizioweb.fizionova.repository"})
@ComponentScan(basePackages = {"com.fizioweb.fizionova.controllers","com.fizioweb.fizionova.services","com.fizioweb.fizionova.repository", "com.fizioweb.fizionova.configuration","com.fizioweb.fizionova.utils"})
@EntityScan(basePackages = {"com.fizioweb.fizionova.models"})
@EnableScheduling

public class FizionovaApplication {


	public static void main(String[] args) {
		SpringApplication.run(FizionovaApplication.class, args);
	}

}
