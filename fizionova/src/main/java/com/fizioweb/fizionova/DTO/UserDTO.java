package com.fizioweb.fizionova.DTO;

import com.fizioweb.fizionova.models.Specialization;
import com.fizioweb.fizionova.models.UserRole;

import java.io.Serializable;

public class UserDTO implements Serializable {
    private String username;

    private UserRole role;

    private String calendarId;

    private Specialization specialization;


    public UserDTO(){

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization calendarName) {
        this.specialization = calendarName;
    }
}
