package com.fizioweb.fizionova.models;

import com.google.api.client.util.DateTime;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class TransmittedEvent implements Serializable {

    private Date startEvent;
    private Date endEvent;
    private String description;
    private String specialization;
    private String doctorName;
    private String status;
    private String eventId;
    private boolean allDay;

    public TransmittedEvent(Date startEvent, Date endEvent, String description, String specialization, String status, String eventId, boolean allDay, String doctorName) {
        this.startEvent = startEvent;
        this.endEvent = endEvent;
        this.description = description;
        this.specialization = specialization;
        this.status = status;
        this.eventId = eventId;
        this.allDay = allDay;
        this.doctorName = doctorName;
    }

    public TransmittedEvent(){

    }

    public Date getStartEvent() {
        return startEvent;
    }

    public void setStartEvent(Date startEvent) {
        this.startEvent = startEvent;
    }

    public Date getEndEvent() {
        return endEvent;
    }

    public void setEndEvent(Date endEvent) {
        this.endEvent = endEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(!(obj instanceof TransmittedEvent)){
            return false;
        }
        TransmittedEvent e = (TransmittedEvent) obj;
        Calendar firstCalendar = Calendar.getInstance();
        Calendar secondCalendar = Calendar.getInstance();
        firstCalendar.setTime(this.getStartEvent());
        secondCalendar.setTime(e.getStartEvent());

        return ((firstCalendar.get(Calendar.DAY_OF_MONTH) == secondCalendar.get(Calendar.DAY_OF_MONTH)) && (firstCalendar.get(Calendar.MONTH) == secondCalendar.get(Calendar.MONTH)) && (firstCalendar.get(Calendar.YEAR) == secondCalendar.get(Calendar.YEAR))
        && (firstCalendar.get(Calendar.HOUR) == secondCalendar.get(Calendar.HOUR)) && (firstCalendar.get(Calendar.MINUTE) == secondCalendar.get(Calendar.MINUTE)) && (this.getDoctorName().equals(e.getDoctorName())));
    }
}
