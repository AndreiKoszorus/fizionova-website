package com.fizioweb.fizionova.models;

import com.google.api.services.calendar.model.Event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CalendarEvent implements Serializable {

    private List<Event> events;
    private String calendarName;

    public CalendarEvent(List<Event> events, String calendarName) {
        this.events = events;
        this.calendarName = calendarName;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    public String getCalendarName() {
        return calendarName;
    }

    public void setCalendarName(String calendarName) {
        this.calendarName = calendarName;
    }
}
