package com.fizioweb.fizionova.models;

import com.mysql.cj.jdbc.Blob;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username", length = 100 , nullable = false, unique = true)
    private String username;

    @Column(name = "password", length = 100, nullable = false)
    private String password;


    @Column
    private String calendarId;

    @JoinColumn(name = "specialization_id", nullable = false)
    @ManyToOne(targetEntity = Specialization.class)
    private Specialization specialization;

    @JoinColumn(name = "role_id", nullable = false)
    @ManyToOne(targetEntity = UserRole.class)
    private UserRole role;

    @Column(name = "active", nullable = false)
    private boolean active;


    public User (){

    }

    public User(String username, String password, UserRole role, String calendarId, Specialization specialization, boolean active) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.calendarId = calendarId;
        this.specialization = specialization;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }

    public String getCalendarId() {
        return calendarId;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
