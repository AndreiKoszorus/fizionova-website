package com.fizioweb.fizionova.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "specializations")
public class Specialization implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", unique = true, length = 100)
    private String name;

    public Specialization(String name) {
        this.name = name;
    }

    public Specialization(){

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
