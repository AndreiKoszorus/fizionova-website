package com.fizioweb.fizionova.models;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "facility")
public class Facility implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private int price;

    @Column
    private String title;

    @Column(length = 500)
    private String description;

    @Column
    @Lob
    private byte[] imgPath;

    public Facility(int price, String title, String description, byte[] imgPath) {
        this.price = price;
        this.title = title;
        this.description = description;
        this.imgPath = imgPath;
    }

    public Facility(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImgPath() {
        return imgPath;
    }

    public void setImgPath(byte[] imgPath) {
        this.imgPath = imgPath;
    }
}
