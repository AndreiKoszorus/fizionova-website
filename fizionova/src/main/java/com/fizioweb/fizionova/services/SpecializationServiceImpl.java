package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.models.Specialization;
import com.fizioweb.fizionova.repository.SpecializationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecializationServiceImpl implements SpecializationService {
    @Autowired
    SpecializationRepository specializationRepository;

    @Override
    public void insertSpecialization(Specialization specialization) {
        if(specialization != null) {
            specializationRepository.save(specialization);
        }
    }

    @Override
    public void deleteSpecialization(Specialization  specialization) {
        if(specialization != null){
            specializationRepository.delete(specialization);
        }
    }

    @Override
    public Specialization findSpecialization(Specialization specialization) {
       return specializationRepository.findByName(specialization.getName());
    }

    public Specialization findSpecializationById(int specializationId){
        return specializationRepository.findById(specializationId);
    }

    @Override
    public List<Specialization> findAll() {
        return specializationRepository.findAll();
    }


}
