package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.models.UserRole;
import com.fizioweb.fizionova.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public UserRole findByUserRole(UserRole userRole) {
        return userRoleRepository.findByRole(userRole.getRole());
    }
}
