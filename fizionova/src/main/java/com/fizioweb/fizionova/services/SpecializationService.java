package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.models.Specialization;

import java.util.List;

public interface SpecializationService {
    void insertSpecialization(Specialization specialization);
    void deleteSpecialization (Specialization specialization);
    Specialization findSpecialization(Specialization specialization);
    List<Specialization> findAll();
    Specialization findSpecializationById(int specializationId);


}
