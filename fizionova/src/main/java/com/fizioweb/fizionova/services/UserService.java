package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.DTO.UserDTO;
import com.fizioweb.fizionova.models.Specialization;
import com.fizioweb.fizionova.models.User;

import java.util.List;

public interface UserService {
    int saveUser(User user);
    List<UserDTO> getAllUsers();
    Boolean deleteUser(String username);
    void updateUser(String username, UserDTO userDTO);
    User findUserByUsername(String username);
    void deleteAllBySpecialization(Specialization specialization);
    UserDTO getUserDTOByUsername(String username);
}
