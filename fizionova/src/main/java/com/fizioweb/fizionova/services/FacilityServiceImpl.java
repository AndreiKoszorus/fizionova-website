package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.models.Facility;
import com.fizioweb.fizionova.repository.FacilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class FacilityServiceImpl implements FacilityService {
    @Autowired
    private FacilityRepository facilityRepository;

    @Override
    public List<Facility> getAll() {
        return facilityRepository.findAll();
    }

    @Transactional
    @Override
    public void deleteFacility(int facilityId) {
        Facility facility = facilityRepository.findFacilityById(facilityId);

        if(facility != null ){
            facilityRepository.delete(facility);
        }

    }

    @Override
    public void updateFacility(int facilityId, Facility updatedFacility) {
        Facility facility = facilityRepository.findFacilityById(facilityId);

        if(facility != null ){
            facility.setDescription(updatedFacility.getDescription());
            facility.setTitle(updatedFacility.getTitle());
            facility.setPrice(updatedFacility.getPrice());
            facility.setImgPath(updatedFacility.getImgPath());

            facilityRepository.save(facility);
        }
    }

    @Override
    public void saveFacility(Facility facility) {
        facilityRepository.save(facility);
    }

    @Override
    public Facility getFacilityById(int id){
        return facilityRepository.findFacilityById(id);
    }
}
