package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.DTO.UserDTO;
import com.fizioweb.fizionova.models.Specialization;
import com.fizioweb.fizionova.models.User;
import com.fizioweb.fizionova.models.UserRole;
import com.fizioweb.fizionova.repository.SpecializationRepository;
import com.fizioweb.fizionova.repository.UserRepository;
import com.fizioweb.fizionova.repository.UserRoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private SpecializationService specializationService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public int saveUser(User user) {
        if(userRepository.findByUsername(user.getUsername()) == null) {
            String encryptedPassword = bCryptPasswordEncoder.encode(user.getPassword());
            user.setPassword(encryptedPassword);
            UserRole userRole = userRoleService.findByUserRole(user.getRole());
            Specialization specialization = specializationService.findSpecialization(user.getSpecialization());
            if (userRole != null && specialization != null) {
                user.setRole(userRole);
                user.setSpecialization(specialization);
                userRepository.save(user);
            } else {
                return 1;
            }
        }
        else{
            return 2;
        }

        return 0;
    }

    @Override
    public List<UserDTO> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDTO> usersDTO = new ArrayList<>();

        for(User user: users){
            usersDTO.add(modelMapper.map(user, UserDTO.class));

        }
        return usersDTO;
    }

    @Override
    @Transactional
    public Boolean deleteUser(String username) {
        User user = userRepository.findByUsername(username);
        if(user != null){
            userRepository.deleteByUsername(username);
            return true;
        }
        return false;
    }

    @Override
    public void updateUser(String username, UserDTO userDTO) {
        User user = userRepository.findByUsername(username);
        if(user != null){
            UserRole userRole = userRoleService.findByUserRole(userDTO.getRole());
            Specialization userSpec = specializationService.findSpecialization(userDTO.getSpecialization());
            user.setCalendarId(userDTO.getCalendarId());
            user.setRole(userRole);
            user.setSpecialization(userSpec);
            userRepository.save(user);
        }
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    @Transactional
    public void deleteAllBySpecialization(Specialization  specialization) {
        if(specialization != null) {
            userRepository.deleteAllBySpecialization(specialization);
        }
    }

    @Override
    public UserDTO getUserDTOByUsername(String username){
        User user = userRepository.findByUsername(username);
        return modelMapper.map(user, UserDTO.class);
    }


}
