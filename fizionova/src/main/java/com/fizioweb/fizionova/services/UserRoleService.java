package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.models.UserRole;

public interface UserRoleService {

    UserRole findByUserRole(UserRole userRole);
}
