package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.models.TransmittedEvent;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface CalendarEventService {

    void run() throws IOException;
    boolean insertEvent(TransmittedEvent transmittedEvent) throws IOException;
    List<TransmittedEvent> getInSync() throws IOException;

}
