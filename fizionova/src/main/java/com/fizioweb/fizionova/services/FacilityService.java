package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.models.Facility;

import java.util.List;

public interface FacilityService {
    List<Facility> getAll();
    void deleteFacility(int facilityId);
    void updateFacility(int facilityId, Facility facility);
    void saveFacility(Facility facility);
    Facility getFacilityById(int id);
}
