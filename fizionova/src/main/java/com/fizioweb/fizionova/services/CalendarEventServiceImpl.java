package com.fizioweb.fizionova.services;

import com.fizioweb.fizionova.models.TransmittedEvent;
import com.fizioweb.fizionova.models.User;
import com.fizioweb.fizionova.repository.UserRepository;
import com.fizioweb.fizionova.utils.CalendarUtils;
import com.fizioweb.fizionova.utils.SendEventsUtil;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.DataStore;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.*;

@Service
public class CalendarEventServiceImpl implements CalendarEventService {
    /**
     * Global instance of the Calendar client.
     */
    private static Calendar client;

    /**
     * Global instance of the event datastore.
     */
    private static DataStore<String> eventDataStore;

    /**
     * Global instance of the sync settings datastore.
     */
    private static DataStore<String> syncSettingsDataStore;

    private List<TransmittedEvent> newEvents = new ArrayList<>();


    @Autowired
    private UserRepository userRepository;

    static {
        try {
            List<String> scopes = Lists.newArrayList(CalendarScopes.CALENDAR);
            client = CalendarUtils.createCalendarClient(scopes);
            eventDataStore = CalendarUtils.getDataStoreFactory().getDataStore("EventStore");
            syncSettingsDataStore = CalendarUtils.getDataStoreFactory().getDataStore("SyncSettings");
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    @Scheduled(fixedRate = 10000)
    @Override
    public void run() throws IOException {
        System.out.println("Emitter size: "+SendEventsUtil.emitters.size());
        List<User> users = userRepository.findAll();
        List<TransmittedEvent> newEvents = new ArrayList<>();
        for (User user : users) {
            String syncToken = syncSettingsDataStore.get(user.getCalendarId());
            newEvents.addAll(syncEvents(syncToken, user, false));
            System.out.println("Sync token : "+syncToken);

        }
        if (newEvents.size() != 0) {
            System.out.println("New Events yeey");
            testEvent(newEvents);
            SendEventsUtil.sendSeeEventsToUi(newEvents);
        }

    }


    @Override
    public List<TransmittedEvent> getInSync() throws IOException {
        List<User> users = userRepository.findAll();
        List<TransmittedEvent> allEvents = new ArrayList<>();
        for (User user : users) {
            allEvents.addAll(syncEvents(null, user, true));

        }
        return allEvents;
    }

    private List<TransmittedEvent> syncEvents(String syncToken, User user, boolean getFullEvents) throws IOException {
        Calendar.Events.List request = client.events().list(user.getCalendarId());
        if (syncToken == null) {
            System.out.println("Performing full sync.");
            Date dateWithoutTime = CalendarUtils.getDateWithoutTimeUsingCalendar();
            request.setTimeMin(new DateTime(dateWithoutTime));
            System.out.println("Date" + dateWithoutTime);

        } else {
            System.out.println("Performing incremental sync.");

            request.setSyncToken(syncToken);
        }
        String pageToken = null;
        Events events = null;
        newEvents.clear();
        do {
            request.setPageToken(pageToken);

            try {
                events = request.execute();
            } catch (GoogleJsonResponseException e) {
                if (e.getStatusCode() == 410) {
                    System.out.println("Invalid sync token, clearing event store and re-syncing.");
                    syncSettingsDataStore.delete(user.getCalendarId());
                    eventDataStore.clear();
                    run();
                } else {
                    throw e;
                }
            }
            if (events != null) {
                List<Event> items = events.getItems();

                if (items.size() == 0) {
                    System.out.println("No new events to sync.");
                } else {

                    TransmittedEvent transmittedEvent;
                    for (Event event : items) {
                        if ("cancelled".equals(event.getStatus())) {
                            transmittedEvent = new TransmittedEvent(null, null, null, user.getSpecialization().getName(), event.getStatus(), event.getId(), false,user.getUsername());
                        } else if (event.getStart().getDateTime() == null) {
                            Date allDay = new Date(event.getStart().getDate().getValue());
                            transmittedEvent = new TransmittedEvent(allDay, allDay, event.getSummary(), user.getSpecialization().getName(), event.getStatus(), event.getId(), true,user.getUsername());
                        } else {
                            Date startDate = new Date(event.getStart().getDateTime().getValue());
                            Date endDate = new Date(event.getEnd().getDateTime().getValue());
                            transmittedEvent = new TransmittedEvent(startDate, endDate, event.getSummary(), user.getSpecialization().getName(), event.getStatus(), event.getId(), false,user.getUsername());
                        }
                        newEvents.add(transmittedEvent);
                        syncEvent(event);
                    }

                }
                pageToken = events.getNextPageToken();
            }
        } while (pageToken != null);

        // Store the sync token from the last request to be used during the next execution.
        if (!getFullEvents) {
            syncSettingsDataStore.set(user.getCalendarId(), events.getNextSyncToken());
        }
        System.out.println("Sync complete.");

        return newEvents;
    }

    @Override
    public boolean insertEvent(TransmittedEvent transmittedEvent) throws IOException {
        DateTime startDateTime = new DateTime(transmittedEvent.getStartEvent(), TimeZone.getTimeZone("UTC"));
        DateTime endDateTime = new DateTime(transmittedEvent.getEndEvent(), TimeZone.getTimeZone("UTC"));

        List<TransmittedEvent> allEvents = this.getInSync();

        for(TransmittedEvent transmittedEvent1: allEvents){
            if(transmittedEvent1.equals(transmittedEvent)){
                return false;
            }
        }

        Event event = new Event().setSummary(transmittedEvent.getDescription())
                .setReminders(new Event.Reminders().setUseDefault(false))
                .setStart(new EventDateTime().setDateTime(startDateTime))
                .setEnd(new EventDateTime().setDateTime(endDateTime));
        User user = userRepository.findByUsername(transmittedEvent.getDoctorName());
        if(user != null) {
            CalendarUtils.createEvent(client, event, user.getCalendarId());
        }
        return true;
    }

    private static void syncEvent(@NotNull Event event) throws IOException {
        if ("cancelled".equals(event.getStatus()) && eventDataStore.containsKey(event.getId())) {
            eventDataStore.delete(event.getId());
        } else {
            eventDataStore.set(event.getId(), event.toString());
        }
    }

    private void testEvent(List<TransmittedEvent> events) {
        for (TransmittedEvent event : events) {

            System.out.println(event.isAllDay() + " " + event.getDescription() + " " + event.getEventId() + " ");
        }
    }


}

